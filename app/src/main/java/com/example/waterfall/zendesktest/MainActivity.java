package com.example.waterfall.zendesktest;

import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import zendesk.core.AnonymousIdentity;
import zendesk.core.Identity;
import zendesk.core.Zendesk;
import zendesk.support.Support;
import zendesk.support.request.RequestActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Zendesk.INSTANCE.init(this, "https://hivechathelp.zendesk.com", "077be34d3e2d5667d935347729e71cced030a99bd5fdcd7a", "mobile_sdk_client_e97f69848eae793b3164");

        Identity identity = new AnonymousIdentity();
        Zendesk.INSTANCE.setIdentity(identity);

        Support.INSTANCE.init(Zendesk.INSTANCE);

        RequestActivity.builder()
                .withRequestSubject("Testing Support SDK 2.0")
                .withTags("2.0", "testing")
                .show(this);
    }
}
